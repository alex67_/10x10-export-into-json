﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10x10_Excel_to_JSON
{
    class Record
    {
        public string id;
        public string login;
        public string SkvoznojNum;
        public string OrgStatus;
        public string UpdateDate;
        public string OrgType;
        public string OrgPravForm;
        public string FullName;
        public string ShortName;
        public string CocialnoZnachimyj;
        public string YurAddrIndex;
        public string YurAddrRegion;
        public string YurAddrRayon;
        public string YurAddrGorod;
        public string YurAddrNasPunkt;
        public string YurAddrMicrorayon;
        public string YurAddrStreet;
        public string YurAddrHouse;
        public string YurAddrCorpus;
        public string YurAddrStroenie;
        public string YurAddrOffice;
        public string YurAddrAbonYaschik;
        public string WebSite1;
        public string WebSite2;
        //Бесплатный телефонный номер для жителей России
        public string localFreePhoneNumber;
        public string Email1;
        public string Email2;
        public string Email3;
        public string Email4;
        public string Category;
        public string INN;
        public string LicenseNumber;
        public string RukovLastName;
        public string RukovFirstName;
        public string RukovMidName;
        public string RukovPosition;
        public string BirthDate;
        public string RukovPhone1N;
        public string RukovPhone1X;
        public string RukovPhone2N;
        public string RukovPhone2X;
        public string RukovFaxN;
        public string RukovFaxX;
        public string RukovCell;
        public string RukovEmail;
        public string RukovHomeEmail;
        public string SecrLastName, SecrFirstName, SecrMidName;
        public string SecrPhone1N;
        public string SecrPhone1X;
        public string SecrPhone2N;
        public string SecrPhone2X;
        public string SecrPhone3N;
        public string SecrPhone3X;
        public string SecrFaxN;
        public string SecrFaxX;
        public string SecrFax2N;
        public string SecrFax2X;
        public string SecrEmail;
        public string SecrEmail2;
        //public string SalesPhone3X;
        //public string SalesPhone4N;
        //public string SalesPhone4X;
        //public string SalesPhone6N;
        //public string SalesPhone6X;
        //public string SalesFax1N;
        //public string SalesFax2N;
        //public string SalesFax3N;
        //public string SalesFax4N;
        //public string SailsEmail1;
        //public string SailsEmail2;
        //public string SailsEmail3;
        //public string SailsEmail4;
        public string MarkRukPhone1N;
        public string MarkRukFaxN;
        public string MarkRukEmail;
        public string MarkNote;
        public string ICQ;
        public string Jabber;
        public string Skype;
        public string Vkontakte;
        public string Facebook;
        public string Twitter;
        public string Instagram;
        public string Odnoklassniki;
        public string TreatmentProfile;
        public string Note;
        public string SetVed;
        public string Association;
        public string kurortmag;
        public string sankurort;
        public string sanatoria;
        public string site_sanatoria;
        public string zdravkurort;
        public string sanatoriy123;
        public string kurort_expert;
        public string kurorti;
        public string vashkurort;
        public string profkurort;
        public string rfsan;
        public string assap;
        public string nashikurorty;
        public string soyuzkurort;
        public string Zdravnica2016;
        public string Revenue2012;
        public string Revenue213;
        public string Revenue2014;
        public string Revenue2015;
        public string Bisnesstat2015;
        public List<BronDep> BronDepList;

    }
    class BronDep
    {
        public string BronDepName;
        public string BronRukPosition;
        public string BronRukLastName;       
        public string BronRukFirstName;
        public string BronRukMidName;
        public string BronRukPhone1N;
        public string BronRukPhone1X;
        public string BronRukPhone2N;
        public string BronRukPhone2X;
        public string BronRukPhone3N;
        public string BronRukPhone3X;
        public string BronRukPhone4N;
        public string BronRukPhone4X;
        public string BronRukPhone5N;
        public string BronRukPhone5X;

        public string BronRukFax1N;
        public string BronRukFax2N;
        public string BronRukFax3N;
        public string BronRukFax4N;

        public string BronRukEmail1;
        public string BronRukEmail2;
        public string BronRukEmail3;
        public string BronRukEmail4;

        //public List<Employee> BronEmployees;
    }
    class Employee
    {
        public string BronDepName;
        public string BronRukLastName;
        public string BronRukPhone1N;
        public string BronRukPhone1X;
        public string BronRukPhone2N;
        public string BronRukPhone2X;
        public string BronRukCell;
        public List<BronDep> Employees;
    }
}
