﻿using System;
using System.Collections.Generic;
using Excel = Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using System.Runtime.InteropServices;
using System.IO;

namespace _10x10_Excel_to_JSON
{
    class Program
    {
        private static Excel.Application app = null;
        private static Excel.Worksheet sheet = null;
        private static Excel.Workbook workbook = null;
        private static Excel.Workbooks workbooks = null;
        private static Excel.Sheets sheets = null;
        static Dictionary<string, int> d;
        static int currentID;
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the path to the excel file name.");
            string filename;
            string default_filename = @"БАЗА СКУ по состоянию на 2 сентября ver 8 сентября 12_00.xlsx";
            string[] files = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory(), "*.xlsx");
            Console.WriteLine(string.Format("Enter \"y\" if the file is in this list: "));
            for (int i = 0; i < files.Length; i++)
            {
                Console.WriteLine(i.ToString() + ": " + files[i]);
            };
            if (Console.ReadLine().ToLower() == "y")
            {
                Console.Write("Enter index: ");
                string number = Console.ReadLine();
                filename = files[int.Parse(number)];
            } else
            {
                Console.WriteLine(string.Format("Enter the file name or press enter if the file name is \"{0}\"", default_filename));
                filename = Console.ReadLine();
                if (filename == "") filename = default_filename;
            }
            initDictionary();
            if (!System.IO.File.Exists(filename))
            {
                Console.WriteLine("The file doesn't exist. Press enter to exit.");
                Console.ReadKey();
                return;
            }
            string fn = Path.GetFileName(filename);
            if (fn == filename || (@".\" + fn) == filename)
                filename = Path.GetFullPath(filename);
            Console.WriteLine("The file is loading...");
            List<Record> json_records = readExcelFile(filename, 200);
            Console.WriteLine("Serialization...");
            
            JsonSerializer serializer = new JsonSerializer();
            serializer.NullValueHandling = NullValueHandling.Ignore;
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(@"json.txt"))
            {
                serializer.Serialize(sw, json_records);
                //sw.Write(jsonStr);
                Console.WriteLine("The file \"json.txt\" is saved.");
            }
            Console.WriteLine("Press key to exit.");
            Console.ReadKey();
        }

        private static void initDictionary()
        {
            d = new Dictionary<string, int>();
            d.Add("Логин КУРОРТ ГУРУ", 1);
             d.Add("Сквозной №", 2);
             d.Add("Статус организации", 3);
             d.Add("Дата обновления информации", 4);
             d.Add("Вид организации", 5);
             d.Add("Организационно-правовая форма учреждения", 6);
             d.Add("Полное наименование организации", 7);
             d.Add("Краткое наименование организации", 8);
             d.Add("Cоциально значимый", 9);
             d.Add("Индекс", 10);
             d.Add("Регион", 11);
             d.Add("Район", 12);
             d.Add("Город", 13);
             d.Add("Населенный пункт", 14);
             d.Add("Жилой район (микрорайон)", 15);
             d.Add("Улица", 16);
             d.Add("Дом", 17);
             d.Add("Корпус", 18);
             d.Add("Cтроение", 19);
             d.Add("Офис / помещение", 20);
             d.Add("Абонентский ящик", 21);
             d.Add("Сайт Официальный 1", 22);
             d.Add("Сайт Официальный 2", 23);
             d.Add("Бесплатный телефонный номер для жителей России", 24);
             d.Add("E-mail 1", 25);
             d.Add("E-mail 2", 26);
             d.Add("E-mail 3", 27);
             d.Add("E-mail 4", 28);
             d.Add("Категория", 29);
             d.Add("ИНН", 30);
             d.Add("Номер лицензии на медицинскую деятельность", 31);
             d.Add("Руководитель учреждения ФИО", 32);
             d.Add("Должность", 33);
             d.Add("Дата рождения", 34);
             d.Add("Руководитель рабочий 1", 35);
             d.Add("Руководитель добавочный 1", 36);
             d.Add("Руководитель рабочий 2", 37);
             d.Add("Руководитель добавочный 2", 38);
             d.Add("Руководитель рабочий 3", 39);
             d.Add("Руководитель добавочный 3", 40);
             d.Add("мобильный", 41);
             d.Add("Руководитель E-mail рабочий", 42);
             d.Add("Руководитель E-mail домашний", 43);
             d.Add("Секретарь ФИО", 44);
             d.Add("Секретарь телефон 1", 45);
             d.Add("Секретарь добавочный 1", 46);
             d.Add("Секретарь телефон 2", 47);
             d.Add("Секретарь добавочный 2", 48);
             d.Add("Секретарь телефон 3", 49);
             d.Add("Секретарь добавочный 3", 50);
             d.Add("Секретарь факс 1", 51);
             d.Add("Секретарь добавочный 4", 52);
             d.Add("Секретарь факс 2", 53);
             d.Add("Секретарь добавочный факс", 54);
             d.Add("Секретарь E-mail 1", 55);
             d.Add("Секретарь E-mail 2", 56);
             d.Add("Бронирование телефон 1", 57);
             d.Add("Бронирование доб 1", 58);
             d.Add("Бронирование телефон 2", 59);
             d.Add("Бронирование доб 2", 60);
             d.Add("Бронирование телефон 3", 61);
             d.Add("Бронирование доб 3", 62);
             d.Add("Бронирование телефон 4", 63);
             d.Add("Бронирование доб 4", 64);
             d.Add("Бронирование телефон 6", 65);
             d.Add("Бронирование доб 6", 66);
             d.Add("Бронирование факс 1", 67);
             d.Add("Бронирование факс 2", 68);
             d.Add("Бронирование факс 3", 69);
             d.Add("Бронирование факс 4", 70);
             d.Add("Бронирование E-mail 1", 71);
             d.Add("Бронирование E-mail 2", 72);
             d.Add("Бронирование E-mail 3", 73);
             d.Add("Бронирование E-mail 4", 74);
             d.Add("Отдел маркетинга телефон", 75);
             d.Add("Отдел маркетинга факс", 76);
             d.Add("Отдел маркетинга E-mail", 77);
             d.Add("Отдел маркетинга Примечание", 78);
             d.Add("ICQ", 79);
             d.Add("Jabber", 80);
             d.Add("Skype", 81);
             d.Add("Vkontakte", 82);
             d.Add("Facebook", 83);
             d.Add("Twitter", 84);
             d.Add("Instagram", 85);
             d.Add("Odnoklassniki", 86);
             d.Add("Профили лечения", 87);
             d.Add("Примечания", 88);
             d.Add("Сетевой/ведомственный", 89);
             d.Add("Членство в ассоциациях", 90);
             d.Add("kurortmag.ru", 91);
             d.Add("sankurort.ru", 92);
             d.Add("sanatoria.ru", 93);
             d.Add("site-sanatoria.ru", 94);
             d.Add("zdravkurort.ru", 95);
             d.Add("sanatoriy123.ru", 96);
             d.Add("kurort-expert.ru", 97);
             d.Add("kurorti.ru", 98);
             d.Add("vashkurort.net", 99);
             d.Add("profkurort.ru", 100);
             d.Add("rfsan.ru", 101);
             d.Add("assap.ru", 102);
             d.Add("nashikurorty.ru", 103);
             d.Add("СОЮЗКУРОРТ", 104);
             d.Add("Здравница 2016", 105);
             d.Add("Выручка за 2012 год (млн руб)", 106);
             d.Add("Выручка за 2013 год (млн руб)", 107);
             d.Add("Выручка за 2014 год (млн руб)", 108);
             d.Add("Выручка за 2015 год (млн руб)", 109);
             d.Add("(Bisnesstat отчет за 2015 год)", 110);
           
        }
        static public string[] parseFIO(string str)
        {
            if (str == null || str=="")
                return null;
            string[] fio;
            str = str.Replace("  ", " ");
            str = str.Replace("  ", " ");
            while (str[0] == ' ')
                str = str.Remove(0, 1);
            fio = str.Split(' ');
            return fio;
        }
        static public string formatNumber(string str)
        {
            /*
             * "RukovCell" : "8-905-037-77-55",
                "id" : "kurort_1844",
                "RukovCell" : "8 978 130 94 60",
                "localFreePhoneNumber" : "8 800 555-15-16",
             * 
             * */

            if (str == null) return str;
            str = str.Trim().Replace("8 (", "+7(").Replace("8(", "+7(");
            str = str.Replace("8 (", "+7(");
            if (str == "") return null;
            if (str[0] == '8')
            {
                //"8 (862) 240-09-73"
                //0 123 456 78 910
                //8 918 449 82 59
                if(!str.Contains("-") && !str.Contains(" "))
                {
                    return string.Format("+7({0}) {1}-{2}-{3}", str.Substring(1,3), str.Substring(4, 3), str.Substring(7, 2), str.Substring(9, 2));
                }
                string[] strs;
                //"8-918-995–46–42"
                str = str.Replace(" ", "-");
                str = str.Replace("–", "-");
                strs = str.Split('-');
                return string.Format("+7({0}) {1}-{2}-{3}", strs[1], strs[2], strs[3], strs[4]);
            }
            else return str;
            //"+7(8552) 71-41-96",
        }
        static public Record row2object(int row)
        {
            Record rec = new Record();
            rec.id = "kurort_" + (currentID++).ToString();
            rec.login = (currentID - 1).ToString();
            rec.SkvoznojNum = toStr(sheet.Cells[d["Сквозной №"]][row].Value);
            rec.OrgStatus = toStr(sheet.Cells[d["Статус организации"]][row].Value);
            rec.UpdateDate = convertDate(sheet.Cells[d["Дата обновления информации"]][row].Value);
            rec.OrgType = toStr(sheet.Cells[d["Вид организации"]][row].Value);
            rec.OrgPravForm = toStr(sheet.Cells[d["Организационно-правовая форма учреждения"]][row].Value);
            rec.FullName = toStr(sheet.Cells[d["Полное наименование организации"]][row].Value);
            rec.ShortName = toStr(sheet.Cells[d["Краткое наименование организации"]][row].Value);
            rec.CocialnoZnachimyj = toStr(sheet.Cells[d["Cоциально значимый"]][row].Value);
            rec.YurAddrIndex = toStr(sheet.Cells[d["Индекс"]][row].Value);
            rec.YurAddrRegion = toStr(sheet.Cells[d["Регион"]][row].Value);
            rec.YurAddrRayon = toStr(sheet.Cells[d["Район"]][row].Value);
            rec.YurAddrGorod = toStr(sheet.Cells[d["Город"]][row].Value);
            rec.YurAddrNasPunkt = toStr(sheet.Cells[d["Населенный пункт"]][row].Value);
            rec.YurAddrMicrorayon = toStr(sheet.Cells[d["Жилой район (микрорайон)"]][row].Value);
            rec.YurAddrStreet = toStr(sheet.Cells[d["Улица"]][row].Value);
            rec.YurAddrHouse = toStr(sheet.Cells[d["Дом"]][row].Value);
            rec.YurAddrCorpus = toStr(sheet.Cells[d["Корпус"]][row].Value);
            rec.YurAddrStroenie = toStr(sheet.Cells[d["Cтроение"]][row].Value);
            rec.YurAddrOffice = toStr(sheet.Cells[d["Офис / помещение"]][row].Value);
            rec.YurAddrAbonYaschik = toStr(sheet.Cells[d["Абонентский ящик"]][row].Value);
            rec.WebSite1 = toStr(sheet.Cells[d["Сайт Официальный 1"]][row].Value);
            rec.WebSite2 = toStr(sheet.Cells[d["Сайт Официальный 2"]][row].Value);
            rec.localFreePhoneNumber = formatNumber(toStr(sheet.Cells[d["Бесплатный телефонный номер для жителей России"]][row].Value));
            rec.Email1 = toStr(sheet.Cells[d["E-mail 1"]][row].Value);
            rec.Email2 = toStr(sheet.Cells[d["E-mail 2"]][row].Value);
            rec.Email3 = toStr(sheet.Cells[d["E-mail 3"]][row].Value);
            rec.Email4 = toStr(sheet.Cells[d["E-mail 4"]][row].Value);
            rec.Category = toStr(sheet.Cells[d["Категория"]][row].Value);
            rec.INN = toStr(sheet.Cells[d["ИНН"]][row].Value);
            rec.LicenseNumber = toStr(sheet.Cells[d["Номер лицензии на медицинскую деятельность"]][row].Value);
            string[] fio = parseFIO(toStr(sheet.Cells[d["Руководитель учреждения ФИО"]][row].Value));
            if (fio != null && fio.Length == 3)
            {
                rec.RukovLastName = fio[0];
                rec.RukovFirstName = fio[1];
                rec.RukovMidName = fio[2];
            }
            rec.RukovPosition = toStr(sheet.Cells[d["Должность"]][row].Value);                
            rec.BirthDate = convertDate(sheet.Cells[d["Дата рождения"]][row].Value);
            rec.RukovPhone1N = formatNumber(toStr(sheet.Cells[d["Руководитель рабочий 1"]][row].Value));
            rec.RukovPhone1X = toStr(sheet.Cells[d["Руководитель добавочный 1"]][row].Value);
            rec.RukovPhone2N =formatNumber(toStr(sheet.Cells[d["Руководитель рабочий 2"]][row].Value));
            rec.RukovPhone2X =toStr(sheet.Cells[d["Руководитель добавочный 2"]][row].Value);
            rec.RukovFaxN =formatNumber(toStr(sheet.Cells[d["Руководитель рабочий 3"]][row].Value));
            rec.RukovFaxX = toStr(sheet.Cells[d["Руководитель добавочный 3"]][row].Value);
            rec.RukovCell = formatNumber(toStr(sheet.Cells[d["мобильный"]][row].Value));
            rec.RukovEmail = toStr(sheet.Cells[d["Руководитель E-mail рабочий"]][row].Value);
            rec.RukovHomeEmail = toStr(sheet.Cells[d["Руководитель E-mail домашний"]][row].Value);
            fio = parseFIO(toStr(sheet.Cells[d["Секретарь ФИО"]][row].Value));
            if (fio != null && fio.Length == 3)
            {
                rec.SecrLastName = fio[0];
                rec.SecrFirstName = fio[1];
                rec.SecrMidName = fio[2];
            }
            rec.SecrPhone1N =formatNumber(toStr(sheet.Cells[d["Секретарь телефон 1"]][row].Value));
            rec.SecrPhone1X =toStr(sheet.Cells[d["Секретарь добавочный 1"]][row].Value);
            rec.SecrPhone2N =formatNumber(toStr(sheet.Cells[d["Секретарь телефон 2"]][row].Value));
            rec.SecrPhone2X =toStr(sheet.Cells[d["Секретарь добавочный 2"]][row].Value);
            rec.SecrPhone3N =formatNumber(toStr(sheet.Cells[d["Секретарь телефон 3"]][row].Value));
            rec.SecrPhone3X =toStr(sheet.Cells[d["Секретарь добавочный 3"]][row].Value);
            rec.SecrFaxN =formatNumber(toStr(sheet.Cells[d["Секретарь факс 1"]][row].Value));
            rec.SecrFaxX =toStr(sheet.Cells[d["Секретарь добавочный 4"]][row].Value);
            rec.SecrFax2N =formatNumber(toStr(sheet.Cells[d["Секретарь факс 2"]][row].Value));
            rec.SecrFax2X = toStr(sheet.Cells[d["Секретарь добавочный факс"]][row].Value);
            rec.SecrEmail = toStr(sheet.Cells[d["Секретарь E-mail 1"]][row].Value);
            rec.SecrEmail2 = toStr(sheet.Cells[d["Секретарь E-mail 2"]][row].Value);


            rec.BronDepList = new List<BronDep>();
            BronDep bronDep = new BronDep();
            bronDep.BronRukPhone1N =formatNumber(toStr(sheet.Cells[d["Бронирование телефон 1"]][row].Value));
            bronDep.BronRukPhone1X =toStr(sheet.Cells[d["Бронирование доб 1"]][row].Value);
            bronDep.BronRukPhone2N =formatNumber(toStr(sheet.Cells[d["Бронирование телефон 2"]][row].Value));
            bronDep.BronRukPhone2X =toStr(sheet.Cells[d["Бронирование доб 2"]][row].Value);
            bronDep.BronRukPhone3N =formatNumber(toStr(sheet.Cells[d["Бронирование телефон 3"]][row].Value));
            bronDep.BronRukPhone3X =toStr(sheet.Cells[d["Бронирование доб 3"]][row].Value);
            bronDep.BronRukPhone4N =formatNumber(toStr(sheet.Cells[d["Бронирование телефон 4"]][row].Value));
            bronDep.BronRukPhone4X =toStr(sheet.Cells[d["Бронирование доб 4"]][row].Value);
            bronDep.BronRukPhone5N =formatNumber(toStr(sheet.Cells[d["Бронирование телефон 6"]][row].Value));
            bronDep.BronRukPhone5X =toStr(sheet.Cells[d["Бронирование доб 6"]][row].Value);

            bronDep.BronRukFax1N =formatNumber(toStr(sheet.Cells[d["Бронирование факс 1"]][row].Value));
            bronDep.BronRukFax2N =formatNumber(toStr(sheet.Cells[d["Бронирование факс 2"]][row].Value));
            bronDep.BronRukFax3N =formatNumber(toStr(sheet.Cells[d["Бронирование факс 3"]][row].Value));
            bronDep.BronRukFax4N =formatNumber(toStr(sheet.Cells[d["Бронирование факс 4"]][row].Value));

            bronDep.BronRukEmail1 = toStr(sheet.Cells[d["Бронирование E-mail 1"]][row].Value);
            bronDep.BronRukEmail2 = toStr(sheet.Cells[d["Бронирование E-mail 2"]][row].Value);
            bronDep.BronRukEmail3 = toStr(sheet.Cells[d["Бронирование E-mail 3"]][row].Value);
            bronDep.BronRukEmail4 = toStr(sheet.Cells[d["Бронирование E-mail 4"]][row].Value);
            rec.BronDepList.Add(bronDep);
            //Skip BronDepList if all fields in bronDep are null.
            bool skip = true;
            foreach (System.Reflection.FieldInfo FInfo in bronDep.GetType().GetFields())
            {
                object firstValue = FInfo.GetValue(bronDep);
                if ((firstValue != null))
                {
                    skip = false; break;
                };
            }

            if (skip)
                rec.BronDepList = null;

            rec.MarkRukPhone1N =formatNumber(toStr(sheet.Cells[d["Отдел маркетинга телефон"]][row].Value));
            rec.MarkRukFaxN =formatNumber(toStr(sheet.Cells[d["Отдел маркетинга факс"]][row].Value));
            rec.MarkRukEmail = toStr(sheet.Cells[d["Отдел маркетинга E-mail"]][row].Value);
            rec.MarkNote = toStr(sheet.Cells[d["Отдел маркетинга Примечание"]][row].Value);
            rec.ICQ = toStr(sheet.Cells[d["ICQ"]][row].Value);
            rec.Jabber = toStr(sheet.Cells[d["Jabber"]][row].Value);
            rec.Skype = toStr(sheet.Cells[d["Skype"]][row].Value);
            rec.Vkontakte = toStr(sheet.Cells[d["Vkontakte"]][row].Value);
            rec.Facebook = toStr(sheet.Cells[d["Facebook"]][row].Value);
            rec.Twitter = toStr(sheet.Cells[d["Twitter"]][row].Value);
            rec.Instagram = toStr(sheet.Cells[d["Instagram"]][row].Value);
            rec.Odnoklassniki = toStr(sheet.Cells[d["Odnoklassniki"]][row].Value);
            rec.TreatmentProfile = toStr(sheet.Cells[d["Профили лечения"]][row].Value);
            rec.Note = toStr(sheet.Cells[d["Примечания"]][row].Value);
            rec.SetVed = toStr(sheet.Cells[d["Сетевой/ведомственный"]][row].Value);
            rec.Association = toStr(sheet.Cells[d["Членство в ассоциациях"]][row].Value);
            rec.kurortmag = toStr(sheet.Cells[d["kurortmag.ru"]][row].Value);
            rec.sankurort = toStr(sheet.Cells[d["sankurort.ru"]][row].Value);
            rec.sanatoria = toStr(sheet.Cells[d["sanatoria.ru"]][row].Value);
            rec.site_sanatoria = toStr(sheet.Cells[d["site-sanatoria.ru"]][row].Value);
            rec.zdravkurort = toStr(sheet.Cells[d["zdravkurort.ru"]][row].Value);
            rec.sanatoriy123 = toStr(sheet.Cells[d["sanatoriy123.ru"]][row].Value);
            rec.kurort_expert = toStr(sheet.Cells[d["kurort-expert.ru"]][row].Value);
            rec.kurorti = toStr(sheet.Cells[d["kurorti.ru"]][row].Value);
            rec.vashkurort = toStr(sheet.Cells[d["vashkurort.net"]][row].Value);
            rec.profkurort = toStr(sheet.Cells[d["profkurort.ru"]][row].Value);
            rec.rfsan = toStr(sheet.Cells[d["rfsan.ru"]][row].Value);
            rec.assap = toStr(sheet.Cells[d["assap.ru"]][row].Value);
            rec.nashikurorty = toStr(sheet.Cells[d["nashikurorty.ru"]][row].Value);
            rec.soyuzkurort = toStr(sheet.Cells[d["СОЮЗКУРОРТ"]][row].Value);
            rec.Zdravnica2016 = toStr(sheet.Cells[d["Здравница 2016"]][row].Value);
            rec.Revenue2012 = toStr(sheet.Cells[d["Выручка за 2012 год (млн руб)"]][row].Value);
            rec.Revenue213 = toStr(sheet.Cells[d["Выручка за 2013 год (млн руб)"]][row].Value);
            rec.Revenue2014 = toStr(sheet.Cells[d["Выручка за 2014 год (млн руб)"]][row].Value);
            rec.Revenue2015 = toStr(sheet.Cells[d["Выручка за 2015 год (млн руб)"]][row].Value);
            rec.Bisnesstat2015 = toStr(sheet.Cells[d["(Bisnesstat отчет за 2015 год)"]][row].Value);
            return rec;
        }
        static private object toStr(Object obj)
        {
            if (obj == null) return null;
                else
                {
                    if (obj is string)
                    {
                        string st = (obj as string);
                        if (st == "" || st == "нет данных") return null;
                        else
                            return (obj as string).Trim();
                    }
                    else {
                        string st= obj.ToString();
                        string res = (st == "") ? null : obj.ToString().Trim();
                        return (res);
                    }
                }
        }
        static private string convertDate(Object obj)
        {
            if (obj is string)
            {
                //"3 сентября 1944"
                return obj as string;
            }
            else
            {
                if (obj != null)
                    return ((DateTime)obj).Date.ToShortDateString();
                else return null;
            }
        }
        static private List<Record> readExcelFile(string filePath, int RowsNumber)
        {
            List<Record> json_records = new List<Record>();
            try
            {
                app = new Excel.Application();
                app.Visible = false;
                workbooks = app.Workbooks;
                workbook = workbooks.Open(filePath);
                sheets = workbook.Sheets;
                sheet = (Excel.Worksheet)sheets[1];
                int firstRow = 5;
                int lastRow = 2341;
                //lastRow = sheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row;
                for (int i = firstRow; i <= lastRow; i++)
                {
                    json_records.Add(row2object(i));
                    Console.Write("\r{0} - rows read", i-4);
                }
                Console.WriteLine();
            }
           catch (Exception ex)
            {
                Console.WriteLine();
                Console.WriteLine("Error occured: "+ex.Message);
            }
            finally
            {
                Console.WriteLine();
                closeExcel();
            }
            return json_records;
        }
        static void closeExcel()
        {
            if(workbooks!=null && workbooks!=null && app!=null)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();

               // Marshal.FinalReleaseComObject(xlRng);
                Marshal.FinalReleaseComObject(sheet);

                workbook.Close(false, Type.Missing, false);
                Marshal.FinalReleaseComObject(workbook);

                app.Quit();
                Marshal.FinalReleaseComObject(app);
            }

        }
        }
    }
